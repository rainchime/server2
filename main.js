import NoteServer from './notes.js';
import Player from './player.js';

const notes = new NoteServer(Player);
notes.on('connect', (player) => {
  console.log('player connected');
});
