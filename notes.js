
import WebSocket from 'ws';

export default class NoteServer {
  constructor(Player) {
    this.server = new WebSocket.Server({ port: 8081 });
    this.server.on('connection', (ws) => (this.handleConnect(ws)));
    this.clients = [];
    this.players = [];
    this.Player = Player;
    this.handlers = {};
  }

  on(type, handler) {
    this.handlers[type] = this.handlers[type] || [];
    this.handlers[type].push(handler);
    return this;
  }

  handleConnect(ws) {
    const client = new Notes(ws);
    this.clients.push(client);
    const player = new this.Player(client);
    this.players.push(player);
    (this.handlers['connect'] || []).some((handler) => handler(player));
  }

}

class Notes {
  constructor(ws) {
    this.ws = ws;
    this.ws.on('message', (json) => (this.handleMessage(json)));
    this.handlers = {};
  }

  handleMessage(json) {
    const message = JSON.parse(json);
    const note = JSON.parse(message.payload);
    console.log('note received:', message.type, note);
    (this.handlers[message.type] || []).some((h) => h(note));
  }

  on(type, handler) {
    this.handlers[type] = this.handlers[type] || [];
    this.handlers[type].push(handler);
  }

  send(type, note) {
    console.log('note sent:', type, note);
    this.ws.send(JSON.stringify({ type, payload: JSON.stringify(note) }));
  }

}
