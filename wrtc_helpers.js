
import WebRTC from 'wrtc';

const configuration = { iceServers: [{
  urls: 'stun:stun.l.google.com:19302'
}] };

export function fromNotes(notes) {
  const { pc, dc } = sendOffer(notes);
  notes.on('wrtc_answer', (note) => receiveAnswer(pc, note));
  notes.on('wrtc_candidate', (note) => receiveCandidate(pc, note));
  return { pc, dc };
}

function sendOffer(notes) {
  const pc = new WebRTC.RTCPeerConnection(configuration);
  pc.onicecandidate = (c) => onIceCandidate(notes, c);
  pc.oniceconnectionstatechange = (e) => onIceConnectionStateChange(notes, e);
  const dc = pc.createDataChannel('main');
  dc.onopen = () => onDCOpen(notes, dc);
  pc.createOffer((desc) => {
    pc.setLocalDescription(desc);
    notes.send('wrtc_offer', desc);
  }, (error) => { throw error; });
  return { pc, dc };
}

function onIceCandidate(notes, candidate) {
  if (!candidate.candidate) return;
  notes.send('wrtc_candidate', candidate);
}

function onIceConnectionStateChange(notes, event) {
  // handle disconnects
}

function onDCOpen(notes, dc) {
  console.log('data channel open');
  dc.onmessage = (e) => onDCMessage(notes, dc, e);
}

function onDCMessage(notes, dc, event) {
  JSON.parse(event.data);
}


export function receiveAnswer(pc, desc) {
  pc.setRemoteDescription(desc);
}

export function receiveCandidate(pc, candidate) {
  pc.addIceCandidate(candidate);
}






