FROM node

WORKDIR /server

COPY . /server

RUN npm install

EXPOSE 80

ENV NAME Server2

# fixes issues with 'less' in alpine
ENV PAGER more

CMD node main.js

