
import * as WRTC from './wrtc_helpers.js';

export default class Streams {
  constructor(notes) {
    this._notes = notes;
    this.registerNotes();
    const { pc, dc } = WRTC.fromNotes(this._notes);
  }

  registerNotes() {
  }

}
