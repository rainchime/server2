
import Streams from './streams.js';

export default class Player {
  
  constructor(notes) {
    this.notes = notes;
    this.streams = new Streams(this.notes);
  }

}
