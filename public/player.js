
import Notes from './notes.js';
import Streams from './streams.js';

export default class Player {
  constructor() {
    this.notes = new Notes('ws://localhost:8081');
    this.streams = new Streams(this.notes);
  }
}
