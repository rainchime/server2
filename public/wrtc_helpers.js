
const configuration = { iceServers: [{
  urls: 'stun:stun.l.google.com:19302'
}] };

export function fromNotes(notes) {
  notes.on('wrtc_offer', (note) => receiveOffer(notes, note));
}

function receiveOffer(notes, desc) {
  const pc = new RTCPeerConnection(configuration);
  pc.onicecandidate = (c) => onIceCandidate(notes, c);
  pc.oniceconnectionstatechange = (e) => onIceConnectionStateChange(notes, e);
  pc.ondatachannel = (e) => onDataChannel(notes, e, pc);
  pc.setRemoteDescription(desc);
  pc.createAnswer((local_desc) => {
    pc.setLocalDescription(local_desc);
    notes.send('wrtc_answer', local_desc);
  }, (error) => { throw error; });

  notes.on('wrtc_candidate', (note) => receiveCandidate(pc, note));
  return pc;
}

function onIceCandidate(notes, candidate) {
  if (!candidate.candidate) return;
  if (candidate.candidate.candidate === '') return;
  notes.send('wrtc_candidate', candidate.candidate);
}

function onIceConnectionStateChange(notes, event) {
  // handle disconnects
}

function onDataChannel(notes, event, pc) {
  const dc = event.channel;
  pc.dc = dc;
  dc.onopen = () => onDCOpen(notes, dc);
}

function onDCOpen(notes, dc) {
  console.log('data channel open');
  dc.onmessage = (e) => onDCMessage(notes, dc, e);
}

function onDCMessage(notes, dc, event) {
  JSON.parse(event.data);
}

function receiveCandidate(pc, candidate) {
  pc.addIceCandidate(candidate.candidate);
}
