
export default class Notes {
  constructor(url) {
    this.ws = new WebSocket(url);
    this.ws.onopen = () => (this.handleConnect());
    this.ws.onmessage = (e) => (this.handleMessage(e));
    this.handlers = {};
  }

  on(type, handler) {
    this.handlers[type] = this.handlers[type] || [];
    this.handlers[type].push(handler);
    return this;
  }

  send(type, note) {
    console.log('note sent:', type, note);
    this.ws.send(JSON.stringify({ type, payload: JSON.stringify(note) }))
  }

  handleConnect() {
    console.log('notes connected');
  }

  handleMessage(e) {
    const message = JSON.parse(e.data);
    const note = JSON.parse(message.payload);
    console.log('note received:', message.type, note);
    (this.handlers[message.type] || []).some((h) => h(note));
  }
}
